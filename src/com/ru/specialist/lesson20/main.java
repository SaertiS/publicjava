package com.ru.specialist.lesson20;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: saertis
 * Date: 25.02.14
 * Time: 21:02
 * To change this template use File | Settings | File Templates.
 */
public class main
{
    public static void showdir(File f, int level)
    {
        if(f.isDirectory())
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < level; i++)
            {
                sb.append("   ");
            }

            System.out.printf("%s%s\n", sb.toString(), f.getName().toUpperCase());
            File[] files = f.listFiles();


            for (File file: files)
            {
                if (file.isDirectory()){
                    showdir(file, level + 1);
                }
            }
            for (File file: files){
                if (file.isFile()){
                    System.out.printf("%s%s %s\n", sb.toString(), file.getName().toLowerCase(), file.lastModified());
                }
            }

        }
    }

    public static void showdir(String s)
    {
        File f = new File(s);
        if(f.exists())
            showdir(f);
    }

    public static void showdir(File file)
    {
        showdir(file, 0);
    }



    public static void main(String[] args)
    {
        showdir("e:\\");
    }
}
