package com.hexlet.s2.lesson2;

import java.util.concurrent.atomic.AtomicInteger;


public class bTree<T extends Comparable<T>> implements ibTree<T>
{
    private ibTree<T> left = null;
    private ibTree<T> right = null;

    private void setRight(ibTree<T> right)
    {
        this.right = right;
    }

    private void setLeft(ibTree<T> left)
    {
        this.left = left;
    }


    private AtomicInteger count = new AtomicInteger(1);

    private final T value;

    public bTree(T value)
    {
        this.value = value;
    }

    @Override
    public ibTree getLeft()
    {
        return left;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ibTree getRight()
    {
        return right;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public T getValue()
    {
        return this.value;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getCount()
    {
        return count.get();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void add(T value)
    {
        //To change body of implemented methods use File | Settings | File Templates.
        if (value.equals(getValue())){
            count.incrementAndGet();
        }else { if (value.compareTo(getValue()) < 0){
                addLeft(value);
            } else {
                addRight(value);
              }
        }
    }

    @Override
    public void forEach(final ibTree.Process<T> process)
    {
        //To change body of implemented methods use File | Settings | File Templates.
        new Thread(){
            @Override
            public void run()
            {
                process.process(getValue(),getCount());
            }
        }.start();
        if (getLeft()!=null)
            getLeft().forEach(process);
        if (getRight()!=null)
            getRight().forEach(process);
    }



    private void addLeft(T value){
        if (getLeft() != null)
            getLeft().add(value);
        else
            setLeft(new bTree<T>(value));
    }

    private void addRight(T value){
        if (getRight()!=null)
            getRight().add(value);
        else
            setRight(new bTree<T>(value));
    }

    public static class Process<T extends Comparable<T>> implements ibTree.Process<T>{

        @Override
        public void process(T value , Integer count)
        {
            System.out.printf("value: %s , count: %s \n", value.toString(),count.toString());
        }
    }

}
