package com.hexlet.s2.lesson2;


public interface ibTree<T extends Comparable<T>>
{
    public ibTree getLeft();

    public ibTree getRight();

    public  T getValue();

    public int getCount();

    public void add(T value);

    public void forEach(Process<T> process);

    public interface Process<T extends Comparable<T>>{
        public void process(T value,Integer count);
    }
}
